/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *  Su tarea consite en crear los 5 requisitos tomando en cuenta que su factor
 * de comparación es la FECHA DE NACIMIENTO
 * @author madar
 */
public class Persona implements Comparable {
    
    private long cedula;
    private String nombre;
    private short diaNacimiento, mesNacimiento,agnoNacimiento;

    public Persona() {
    }

    /**
     *
     * @param cedula
     * @param nombre
     * @param diaNacimiento
     * @param mesNacimiento
     * @param agnoNacimiento
     * @throws RuntimeException
     */
    public Persona(long cedula, String nombre, int diaNacimiento, int mesNacimiento, int agnoNacimiento)throws RuntimeException
    {
        if(cedula == 0 || diaNacimiento == 0 || mesNacimiento == 0 ||agnoNacimiento == 0 ){
            throw  new RuntimeException("hay un dato vacio, revisar cedula, dia de nacimiento, mes de nacimiento o agno de nacimiento");
        }
        
        if(nombre == null ||  nombre.isEmpty()){
            throw new RuntimeException("Dato vacío para el nombre");
        }
        
        if(diaNacimiento > 31 || diaNacimiento < 1)
        {
            throw  new RuntimeException("el dia de nacimiento no corresponde a un valor aceptado");
        }
        
        if(mesNacimiento>12 || mesNacimiento<1){
            throw new RuntimeException("el mes de nacimiento no corresponde a un valor aceptado");
        }
        if(agnoNacimiento>2021){
            throw new RuntimeException("el año de nacimiento no corresponde a un valor aceptado");
        }
        try{
          
        this.cedula = cedula;
        this.nombre = nombre;
        this.diaNacimiento = (short)diaNacimiento;
        this.mesNacimiento = (short)mesNacimiento;
        this.agnoNacimiento = (short)agnoNacimiento;
        }catch(java.lang.NumberFormatException e){
                     throw new RuntimeException("dato no válido "+e.getMessage());
                }
    }
    
    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getDiaNacimiento() {
        return diaNacimiento;
    }

    public void setDiaNacimiento(short diaNacimiento) {
        this.diaNacimiento = diaNacimiento;
    }

    public short getMesNacimiento() {
        return mesNacimiento;
    }

    public void setMesNacimiento(short mesNacimiento) {
        this.mesNacimiento = mesNacimiento;
    }

    public short getAgnoNacimiento() {
        return agnoNacimiento;
    }

    public void setAgnoNacimiento(short agnoNacimiento) {
        this.agnoNacimiento = agnoNacimiento;
    }
    
    
    //vamos a realizar el metodo equals comparando la fecha de nacimiento

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.diaNacimiento != other.diaNacimiento) {
            return false;
        }
        if (this.mesNacimiento != other.mesNacimiento) {
            return false;
        }
        if (this.agnoNacimiento != other.agnoNacimiento) {
            return false;
        }
        return true;
    }


    @Override    
    public int compareTo(Object obj) {    
        if (this == obj) { // Compara si los dos objetos están apuntando a la misma dirección de memoria-> los objetos son iguales
            return 0;
        }

        final Persona other = (Persona) obj; //es necesario para trabajar con la subclase
        /*
            ESTE COMPARETO ES CANDIDO(ingenuos-A FUERZA BRUTA) , POR QUE DEBERÍA SER UNA RESTA 
        */
        
        if(this.diaNacimiento==other.diaNacimiento && this.mesNacimiento == other.mesNacimiento && this.agnoNacimiento == other.agnoNacimiento)
            return 0;
        if(this.agnoNacimiento > other.agnoNacimiento)
            return 1;
        if(this.mesNacimiento > other.mesNacimiento)
            return 1;
        if(this.diaNacimiento > other.diaNacimiento)
            return 1;
      return -1;
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", diaNacimiento=" + diaNacimiento + ", mesNacimiento=" + mesNacimiento + ", agnoNacimiento=" + agnoNacimiento + '}';
    }  

    public int setCedula() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
