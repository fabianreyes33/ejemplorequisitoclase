/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Persona;
import java.util.Scanner;

/**
 * Su tarea consiste en crear dos objetos de la clase Persona y probar todos los requisitos
 * @author madar
 */

public class PruebaPersona {
 
    public static void main(String[] args) {
    
        Scanner teclado = new Scanner(System.in);
  //      Persona p = new Persona(1004925266, "fabian", 12, 9, 1999);
   //    Persona q = new Persona(1004925275, "maicol", 13, 10, 2000); aqui creamos los bjetos persona asignando los atributos
   Persona p1 = new Persona(); // creamos los objetos p1, p2 y pediremos los atributos al usuario en tiempo de ejecicion.
   Persona p2 = new Persona(); 
      try{ 
       /**
        * vamos a pedir por consola que se digiten los datos de cada persoan 
        */
       System.out.println("dijite el nombre de la primera persona");
       String nombreP1;
       nombreP1 = teclado.nextLine();
       p1.setNombre(nombreP1); 
       
       System.out.println("dijite la cedula de la primera persona");
       long cedulaP1 = teclado.nextLong();
       p1.setCedula(cedulaP1);
       
       System.out.println("dijite eldia de nacimiento de la primera persona");
       int diaNp1 = teclado.nextInt();
       p1.setDiaNacimiento((short)diaNp1);
       
       System.out.println("dijite el mes de nacimiento de la primera persona");
       int mesNp1 = teclado.nextInt();
       p1.setMesNacimiento((short)mesNp1);
       
       System.out.println("dijite el anio de nacimiento de la primera persona");
       int anioNp1 = teclado.nextInt();
       p1.setAgnoNacimiento((short)anioNp1);
       
        /**
         * vamos a conocer los atributos de la primera persona por medio de un to String
         */
        System.out.println(p1.toString());
        /**
         * procedemos a pedir los datos de la segunda persona 
         */
        teclado.nextLine();
       
       System.out.println("dijite el nombre de la segunda persona");
       String nombreP2 = teclado.nextLine();
        p2.setNombre(nombreP2); 
        
       System.out.println("dijite la cedula de la segunda persona");
       long cedulaP2 = teclado.nextLong();
       p2.setCedula(cedulaP2);
       
       System.out.println("dijite eldia de nacimiento de la segunda persona");
       int diaNp2 = teclado.nextInt();
       p2.setDiaNacimiento((short)diaNp2);
       
       System.out.println("dijite el mes de nacimiento de la segunda persona");
       int mesNp2 = teclado.nextInt();
       p2.setMesNacimiento((short)mesNp2);
       
       System.out.println("dijite el anio de nacimiento de la segunda persona");
       int anioNp2 = teclado.nextInt();
       p2.setAgnoNacimiento((short)anioNp2);
       
       /**
        * vamos a conocer los atributos de la segunda persona por medio de un to Strig
        */
        System.out.println(p2.toString());
        
      }catch(java.util.InputMismatchException e) // Si algo pasa
        {
            System.err.println("Se esperaba datos String,float,int,int,int  respectivamente");
        }
      
        
        
        if(p1.equals(p2))
            System.out.println("el objeto p1 es igual al objeto p2");
        else{
            System.out.println("el objeto p1 no es igual al objeto p2");
        }
        int comparador=((Comparable)p1).compareTo(p2);
        
        String msg="Son iguales p1: "+p1.getNombre()+" y p2: "+p2.getNombre();
       
        switch(comparador)
        {
            case 1:{
                msg="p1: "+p1.getNombre()+" es menor que p2: "+p2.getNombre();
                break;
            }
            case -1:
                msg="p1: "+p1.getNombre()+" es mayor que p2: "+p2.getNombre();
                break;
            }
        
        System.out.println(msg);
        
        //Requisito 5:
        System.out.println(p1.toString());
        System.out.println(p2.toString());
        
        
    }
    
}